#!/bin/bash

# Find package manager
if yum --version 2>&1 >/dev/null; then
    PACKAGE_MANAGER="yum"
elif apt-get --version 2>&1 >/dev/null; then
    PACKAGE_MANAGER="apt-get"
else
    echo "Error can't find package manager"
    exit 1;
fi

echo "Using $PACKAGE_MANAGER"

# Update package list for apt-get
if [ PACKAGE_MANAGER == "apt-get" ]; then
    sudo $PACKAGE_MANAGER update
fi

# Install Git if necessary
if git --version 2>&1 >/dev/null; then
    echo "Git is already installed"
else
    sudo $PACKAGE_MANAGER -y install git
fi

# Install NVM
git clone https://github.com/creationix/nvm.git ~/.nvm && cd ~/.nvm && git checkout `git describe --abbrev=0 --tags`
. ~/.nvm/nvm.sh
echo -e "export NVM_DIR=\"$HOME/.nvm\"\n[ -s \"$NVM_DIR/nvm.sh\" ] && . \"$NVM_DIR/nvm.sh\"" >> ~/.profile

# Reload .profile to start using NVM without restarting
source $HOME/.profile

# Install Node (and npm)
nvm install node
nvm alias default node

# Install Gulp
npm install -g gulp
