# Vagrant VM Setup

Run `vagrant up` to create and configure a guest machine based on the provided `Vagrantfile`.

To use a different VM box, delete the file `Vagrantfile`, run `vagrant init [box-name] [box-url]` to setup a different machine, and then run `vagrant up`.

Run `vagrant ssh` to SSH into the Vagrant machine.

To install all necessary tools, run the script in `install.sh`.

On Windows, Git may need to be configured to accept line endings without carriage returns (\r). If saving files with Unix line endings results in "warning: LF will be replaced by CRLF," run the following command to disable the CRLF conversion:
```bash
git config --global core.autocrlf false
```
